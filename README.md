# Description of hurtle

At the moment, I have only managed to deploy a cluster on our local OpenStack installation even though I have admin access on Switch Engine's OpenStack tenant. For this reason, I only want to explain which are the main settings within the config files. One more thing: I explain everything for deployment on OpenShift 3 which itself includes the PaaS system Docker.

## Introduction
hurtle is an orchestration framework consisting of 3 parts which guide a service through its whole lifetime from design to disposal. The current version runs (amongst others) with OpenShift 3 and can deploy pre-configured/-created Docker images. These docker images then can initialise the deployment of a Heat Orchestration Template, for instance on OpenStack.

## Overview 
In the hurtle ecosystem, a service needs to be orchestrated by a service orchestrator (SO). A SO is initialised by a service manager (SM) which can hold several SO of the same type. (though possibly with different given attributes)

SO and SM are the two main components in hurtle which access the cloud controller (CC) over an API. (CC_API) The configuration of the SO's interface is handled in the JSON file service_manifest.json, located in the data subfolder of the service bundle. (service_manifest.json: see below) This file is referenced within the sm.cfg file within the etc folder. The sm.cfg configures the SM in 4 sections. (see below)

The only other thing that has to be created is the SO's implementation in the so.py file. (bundle/wsgi/so.py) The SO has to implement an interface for the individual service states. (design, deployment, provisioning, updating, status, notification and disposal) These states will be called upon creation of the SO instance or by user request respectively. Though it's enough if only the required states are programmed. If at a state transition, some attributes have to be inserted into the instance, a file etc/service_params.json can be created with those parameters. (However, I won't go into that in this short documentation)

For different types of services, there can be different SM. (It could also be solved by one big SO which takes many attributes and acts accordingli - at the moment, this SO is implemented that way, though I will change it to multiple smaller SO/SM combinations) Let's say there is a Hadoop cluster and a Spark cluster to be orchestrated. This means that 2 different SO and SM have to be created with individual implementation and configuration files. (so.py, service_manifest.json, sm.cfg)

In the next paragraphs, I'm going to explain the according configuration / implementation files. If something should remain unclear, feel free to send me an email. In this short description, I will cite from some of the documentation of hurtle: https://github.com/icclab/hurtle/blob/master/docs/how_to_write_a_hurtle_service.md

## service_manifest.json
service_manifest.json shows the SM the individual configuration and the access information of the SO. The configuration entries are the following:
service_type: this is a URL that uniquely defines the service type.
service_description: this is a plain text field that provides a short description of the service.
service_attributes: this is a dictionary of attributes that the service exposes. You can configure the SO with these attributes; they will be passed on to the SO by the SM.
service_endpoint: this is the endpoint where the service manager takes requests. In the example the service manager will listen to local host on port 8888. All requests are to be sent to the URL fragment of '/haas/'.
depends_on: this is a dictionary of services that are dependencies of the service you are defining. In this example, the service has no dependencies. For how to include service dependencies see the "Service Dependencies and Composition" under the above link in the hurtle documentation.

## sm.cfg
The SM configuration is also described under https://github.com/icclab/hurtle_sm . Here, I will bring the most basic description that is needed for thi SM. All the SM is configured with the sm.cfg file; there is no additional coding involved.
The sm.cfg consists of 4 sections which I would like to explain shortly.
The [general] section has some information about logging. You can log either locally or in a logging server. (latter is configured with graylog_{api,port})
[service_manager] contains configurations for the actual SM including deployment information. manifest has to point to the service_manifest.json file of the SO so that its information (such as endpoint, description or attributes) are known to the SM. bundle_location tells the SM where the SO is located. (Docker image on docker hub) design_uri points to the endpoint of the keystone service where the deployment is to take place.
[service_manager_admin] might register the service at the given keystone installation if register_service is set to true. In this case, the login information needs to be set, i.e. service_token, service_tenant_name and region have to be set to a valid keystone token, the according tenant name and the region where the service should be registered, respectively.
The section [cloud_controller] deals with the CC_API. Here, the login information of the OpenShift installation will have to be set. nb_api, user and pwd need to be filled with OpenShift's URL, its username and that user's password. If the connection cannot be established, wait_time and max_attempts will define how many retries shall be done with which waiting time between two retries.

## so.py
This is the file where the actual implementation of the SO is done. I'm not going to go into the details here. I just want to mention that this file will be included by the bundle/wsgi/application file. application is the very same file which will be executed on OpenShift as the SO instance. It will then delegate the requests (including the given attributes) to the so.py file. The given attributes can be accessed in the attributes dictionary in the individual methods.
