import requests
import time

token = '5c561faee5fd40efab9458c603be644e'
heads = {'X-Auth-Token':token, 'X-Tenant-Name':'mesz', 'Content-Type':'text/occi', 'Accept':'text/occi'}
#host = "http://192.168.99.100:32772"
host = 'http://localhost:8080'
#host = 'http://sohaas910c865a08de7861.test.apps.opsv3.cloudcomplab.ch'
# host = 'http://10.88.88.128:8080'  # for use with docker executed images

start=time.time(); r = requests.get(host+'/-/', headers=heads); r.headers; stop=time.time(); stop-start

heads['Category']='orchestrator; scheme="http://schemas.mobile-cloud-networking.eu/occi/service#"'
start=time.time(); r = requests.put(host+'/orchestrator/default', headers=heads); r.headers; stop=time.time(); stop-start

heads['Category']='deploy; scheme="http://schemas.mobile-cloud-networking.eu/occi/service#"'
heads['X-OCCI-Attribute']='icclab.haas.rootfolder="/Users/puenktli/Documents/Coding/PycharmProjects/HaaS/bundle/data",icclab.haas.slave.number="1",icclab.haas.master.floatingip="false",icclab.haas.master.slaveonmaster="false",icclab.haas.master.sshkeyname="mesz MNMBA",icclab.haas.debug.savetemplatetolocalpath="/Users/puenktli/Desktop/currentTemplate.yaml"'
start=time.time(); r = requests.post(host+'/orchestrator/default?action=deploy', headers=heads); r.headers; stop=time.time(); stop-start

r = requests.get(host+'/orchestrator/default', headers=heads); r.headers

heads['Category']='provision; scheme="http://schemas.mobile-cloud-networking.eu/occi/service#"'
start=time.time(); r = requests.post(host+'/orchestrator/default?action=provision', headers=heads); r.headers; stop=time.time(); stop-start

r = requests.get(host+'/orchestrator/default', headers=heads); r.headers

r = requests.delete(host+'/orchestrator/default', headers=heads)

"""

curl -v -X GET http://192.168.99.100:32771/-/ -H 'Accept: text/occi' -H 'X-Auth-Token: 73c0b22f89a44fb297ae4f1d98f7b6e4' -H 'X-Tenant-Name: mesz'

curl -v -X POST http://127.0.0.1:8888/haas/ -H 'Category: haas; scheme="http://schemas.cloudcomplab.ch/occi/sm#"; class="kind";' -H 'content-type: text/occi' -H 'X-Auth-Token: 4bc2b7087903461eb53f1dbbc477a7d8' -H 'X-Tenant-Name: mesz@zhaw.ch'

curl -v -X POST http://160.85.231.190:8888/haas/ -H 'Category: haas; scheme="http://schemas.hurtle.it/occi/sm#"; class="kind";' -H 'content-type: text/occi' -H 'x-tenant-name: YOUR_TENANT_NAME' -H 'x-auth-token: YOUR_KEYSTONE_TOKEN'


curl -v -X POST http://160.85.231.190:8888/orchestrator/default?action=deploy -H 'Category: deploy; scheme="http://schemas.mobile-cloud-networking.eu/occi/service#"' -H 'X-OCCI-Attribute: icclab.haas.master.image = "Ubuntu-Trusty-1404-7-10-2015"' -H 'X-OCCI-Attribute: icclab.haas.slave.image = "Ubuntu-Trusty-1404-7-10-2015"'

"""